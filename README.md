# Overview

The `vesctl` tool is a configuration command line utility that allows users to create, debug and diagnose Volterra Services configuration. It is modelled after Volterra API. All Volterra commands are available via `vesctl`.

## Prerequisites

* Volterra account [signup](https://console.ves.volterra.io/signup/account)
* vesctl binary [binary](https://gitlab.com/volterra.io/vesctl/blob/main/README.md#installing-vesctl-binary)
* [Volterra API credentials](https://gitlab.com/volterra.io/vesctl#obtaining-api-credentials-from-volterra-console)

## Installing vesctl binary

Download the latest Mac OS or Linux `vesctl` binary from
* https://gitlab.com/volterra.io/vesctl/-/releases

* For MacOS
`curl -LO "https://vesio.azureedge.net/releases/vesctl/$(curl -s https://downloads.volterra.io/releases/vesctl/latest.txt)/vesctl.darwin-amd64.gz"`
* For linux `curl -LO "https://vesio.azureedge.net/releases/vesctl/$(curl -s https://downloads.volterra.io/releases/vesctl/latest.txt)/vesctl.linux-amd64.gz"`

By proceeding with the installation, download and/or access and use, as applicable, of the Volterra software, and/or Volterra platform, you acknowledge that you have read, understand, and agree to be bound by this [agreement](https://f5.com/pdf/customer-support/eusa.pdf).

Note: In case of MacOS, ensure that you allow vesctl app from security settings.

## Running vesctl

```
$ vesctl --help
A command line utility to interact with ves service.

Usage:
  vesctl [command]

Available Commands:
  completion    Generate completion script
  configuration Configure object
  help          Help about any command
  request       Execute Commands
  site          Manage site creation through view.aws_vpc apis
  version       Print build version

Flags:
  -a, --cacert string         Server CA cert file path
  -c, --cert string           Client cert file path
      --config string         A configuration file to use for API gateway URL and credentials (default "/Users/user1/.vesconfig")
  -h, --help                  help for vesctl
  -k, --key string            Client key file path
      --outfmt string         Output format for command
  -o, --output string         Output dir for command (default "./")
      --p12-bundle string     Client P12 bundle (key+cert) file path. Any password for this file should be in environment variable VES_P12_PASSWORD
  -u, --server-urls strings   API endpoint URL (default [http://localhost:8001])
      --show-curl             Emit requests from program in CURL format
      --timeout int           Timeout (in seconds) for command to finish (default 5)

Use "vesctl [command] --help" for more information about a command.
```

Before you can start using `vesctl`, you need proper credentials. One of the methods to give credentials is a config file. This file can be given on command line as an option or can be present in users home directory.

```
$HOME/.vesconfig
```

There are two pertinent options in config file:

* `server-urls` URL where the user would access Volterra console. For individual user it will be `https://console.ves.volterra.io/api`. For enterprise user it will be `https://acmecorp.console.ves.volterra.io/api`.
* `p12-bundle` Is the P12 API credentials downloaded from volterra console e.g. acmecorp.console.ves.volterra.io.api-creds.p12

```
$ cat /Users/user1/.vesconfig
server-urls: https://acmecorp.console.ves.volterra.io/api
p12-bundle: /Users/user1/acmecorp.console.ves.volterra.io.api-creds.p12

```

## Obtaining API Credentials from Volterra Console

P12 file can be downloaded from Volterra console. 

* Log in to Volterra console
* Go to **General** namespace
* Under **Personal Management**
* Select **My Credentails**
* Create credentials with type `API certificate`

Create form will ask for password for the P12 certificate. This password is required whenever P12 files is used so don't forget this passworod. Download P12 file to your computed or let browser download it to default directory.

```
$ ls ~/Downloads/*.p12*
/Users/user1/Downloads/acmecorp.console.ves.volterra.io.api-creds.p12
```

This downloaded P12 file can be used to access Volterra using `vesctl`. However when `vesctl` binary needs use the P12 file it will need the password to access certificate and private key inside P12. This password can be set in environment variable.

```
$ export VES_P12_PASSWORD=myp12password
```

Alternatively, you can extract the certificate and private key out of the P12 file:
```
$ openssl pkcs12 -in ~/acmecorp.console.ves.volterra.io.api-creds.p12 -nodes -nokeys -out $HOME/vescred.cert
Enter Import Password:
MAC verified OK
$ openssl pkcs12 -in ~/acmecorp.console.ves.volterra.io.api-creds.p12 -nodes -nocerts -out $HOME/vesprivate.key
Enter Import Password:
MAC verified OK
```

You can add certificate and private key file path to config file `$HOME/.vesconfig`

```
$ cat $HOME/.vesconfig
server-urls: https://acmecorp.console.ves.volterra.io/api
key: /Users/user1/vesprivate.key
cert: /Users/user1/vescred.cert
```

**NOTE**: `vesctl` gives an error if p12-bundle option *and* cert & key options are given in the config file. Add either `p12-bundle` option or `cert` & `key` options in the config file.


## vesctl Basic Syntax and Options

```
$ vesctl --help
A command line utility to interact with ves service.

Usage:
  vesctl [command]

Available Commands:
  completion    Generate completion script
  configuration Configure object
  help          Help about any command
  request       Execute Commands
  site          Manage site creation through view.aws_vpc apis
  version       Print build version

Flags:
  -a, --cacert string         Server CA cert file path
  -c, --cert string           Client cert file path
      --config string         A configuration file to use for API gateway URL and credentials (default "/Users/user1/.vesconfig")
  -h, --help                  help for vesctl
  -k, --key string            Client key file path
      --outfmt string         Output format for command
  -o, --output string         Output dir for command (default "./")
      --p12-bundle string     Client P12 bundle (key+cert) file path. Any password for this file should be in environment variable VES_P12_PASSWORD
  -u, --server-urls strings   API endpoint URL (default [http://localhost:8001])
      --show-curl             Emit requests from program in CURL format
      --timeout int           Timeout (in seconds) for command to finish (default 5)

Use "vesctl [command] --help" for more information about a command.
```

Basic syntax that `vesctl` follows is
```
vesctl high-level-command verb ...
```
High-level-command 

* `help` is available at every level and it prints what next level of options are available. It is also displayed on error at given level.
* `configuration` is cmd is used for CRUD operations on configuration objects.
* `request` is used to implement custom commands and custom RPC access to Volterra API.
* `site` site command is used to manage cloud site through objects aws_vpc_site, azure_vnet_site

For most of cases in both `configuration` and `request` commands final parameter will be in form of YAML files. Schema for these YAML files can be found in [Volterra API reference](https://https://volterra.io/docs/api)

## `configuration` Command

```
$ vesctl configuration
Configure object

Usage:
  vesctl configuration [command]

Aliases:
  configuration, cfg, c

Examples:
vesctl configuration create virtual_host

Available Commands:
  create      Create configuration object
  delete      Delete configuration object
  get         Get configuration object
  list        List configuration objects
  replace     Replace configuration object
  status      Status of configuration object
```

Here you can see available verbs are `create`, `get`, `list` etc.
Generic format for configuration command is

```
vesctl configuration verb kind name -n namespace
```


To see all available kinds

```
./vesctl configuration create --help
Create configuration object

Usage:
  vesctl configuration create [command]

Examples:
vesctl configuration create virtual_host -i <file>

Available Commands:
  advertise_policy         Create advertise_policy
  app_setting              Create app_setting
  app_type                 Create app_type
  bgp                      Create bgp
  bgp_asn_set              Create bgp_asn_set
  cluster                  Create cluster
  contact                  Create contact
  customer_support         Create customer_support
  discovery                Create discovery
  endpoint                 Create endpoint
  fast_acl                 Create fast_acl
  fast_acl_rule            Create fast_acl_rule
  fleet                    Create fleet
  healthcheck              Create healthcheck
  ip_prefix_set            Create ip_prefix_set
  kms_key                  Create kms_key
  kms_policy               Create kms_policy
  kms_policy_rule          Create kms_policy_rule
  namespace                Create namespace
  namespace_role           Create namespace_role
  network_connector        Create network_connector
  network_firewall         Create network_firewall
  network_interface        Create network_interface
  network_policy           Create network_policy
  network_policy_rule      Create network_policy_rule
  network_policy_set       Create network_policy_set
  policer                  Create policer
  protocol_policer         Create protocol_policer
  registration             Create registration
  role                     Create role
  route                    Create route
  secret_management_access Create secret_management_access
  secret_policy            Create secret_policy
  secret_policy_rule       Create secret_policy_rule
  service_policy           Create service_policy
  service_policy_rule      Create service_policy_rule
  service_policy_set       Create service_policy_set
  site                     Create site
  site_mesh_group          Create site_mesh_group
  token                    Create token
  user                     Create user
  virtual_host             Create virtual_host
  virtual_k8s              Create virtual_k8s
  virtual_network          Create virtual_network
  virtual_site             Create virtual_site
  waf                      Create waf
  waf_rules                Create waf_rules

Flags:
  -h, --help                help for create
  -i, --input-file string   File containing CreateRequest contents

Global Flags:
  -a, --cacert string         Server CA cert file path
  -c, --cert string           Client cert file path
      --config string         A configuration file to use for API gateway URL and credentials (default "/Users/user1/.vesconfig")
  -k, --key string            Client key file path
      --outfmt string         Output format for command
  -o, --output string         Output dir for command (default "./")
      --p12-bundle string     Client P12 bundle (key+cert) file path. Any password for this file should be in environment variable VES_P12_PASSWORD
  -u, --server-urls strings   API endpoint URL (default [http://localhost:8001])
      --show-curl             Emit requests from program in CURL format
      --timeout int           Timeout (in seconds) for command to finish (default 5)
```

### `configuration create` Command

following example creates advertise policy in documentation namespace for acmecorp tenant:

(`string:///<base64>` base64 here represents certificate and blindfold secret. Deleted from output to remove clutter)

```
$ cat advertise_policy.yaml
metadata:
  name: advertise-on-public
  namespace: documentation
spec:
  port: 443
  protocol: TCP
  tlsParameters:
    commonParams:
      tlsCertificates:
      - certificateUrl: string:///<Base64>
        privateKey:
          blindfoldSecretInfo:
            location: string:///<Base64>
  where:
    virtualNetwork:
      ref:
      - kind: virtual_network
        name: public
        namespace: shared
        tenant: ves-io

$ vesctl configuration create advertise_policy -i advertise_policy.yaml
metadata:
  annotations: {}
  labels: {}
  name: advertise-on-acmecrop-vsite-1
  namespace: documentation
spec:
  where:
    virtualSite:
      ref:
      - kind: virtual_site
        name: acmecrop-vsite1
        namespace: documentation
        tenant: acmecorp
        uid: ffffffff-ffff-ffff-ffff-ffffffffffff
systemMetadata:
  creationTimestamp: "2019-11-02T13:10:47.549939800Z"
  finalizers: []
  tenant: acmecorp
  uid: 1a2c442a-b0fa-4439-9434-9de2521bfd3b
```

### `configuration list` command

To list advertise_policy that was created above:

```
$ vesctl configuration list advertise_policy --namespace documentation --outfmt yaml
items:
- labels: {}
  name: advertise-on-acmecrop-vsite-1
  namespace: documentation
  statusSet: []
  tenant: acmecorp
  uid: c3efffae-f8de-4d6c-9946-9924343605d4

----
- labels: {}
  name: advertise-on-public
  namespace: documentation
  statusSet: []
  tenant: acmecorp
  uid: 04b97502-5930-4d40-9c1e-48b2f7ee04e0

----
```

### `configuration get` command

To get advertise_policy that was created above:

```
$ vesctl configuration get advertise_policy advertise-on-public --namespace documentation
metadata:
  annotations: {}
  labels: {}
  name: advertise-on-public
  namespace: documentation
resourceVersion: "80877291"
spec:
  port: 443
  protocol: TCP
  tlsParameters:
    commonParams:
      cipherSuites: []
      tlsCertificates:
      - certificateUrl: string:///<Base64>
        privateKey:
          blindfoldSecretInfo:
            location: string:///<Base64>
  where:
    virtualNetwork:
      ref:
      - kind: virtual_network
        name: public
        namespace: shared
        tenant: ves-io
status: []
```

### `configuration replace` command

To change HTTP to HTTPS in advertise policy. Advertise bookinfo productpage on all sites represented by the virtual site acmecrop-vsite1. It is advertised as HTTPS on site local network. VIP is automatically chosen by the system:

```
$ cat advertise_policy.yaml
metadata:
  name: advertise-on-acmecrop-vsite-1
  namespace: documentation
spec:
  port: 443
  protocol: TCP
  tlsParameters:
    commonParams:
      tlsCertificates:
      - certificateUrl: string:///<Base64>
        privateKey:
          blindfoldSecretInfo:
            location: string:///<Base64>
  where:
    virtualSite:
      ref:
      - kind: virtual_site
        name: acmecrop-vsite1
        namespace: documentation
        tenant: acmecorp
$ vesctl configuration replace advertise_policy -n documentation -i advertise_policy.yaml
```

### `configuration delete` command

To delete a advertise policy that advertise product page in public network:

```
$ vesctl configuration delete advertise_policy advertise-on-public --namespace documentation
```

### `configuration status` command

1. for a given object configuration status command gives the global status of the object aggregated in volterra global controller. This is applicable only to certain objects viz. discovery, endpoint, site.
2. with the option --at-site, configuration status command gives the status of the object at a given site. The status includes whether the object was validated and installed on that site.

example of both these forms in given below

```
# vesctl --config ~/.vesconfig.demo1.testcorp configuration status endpoint ves-io-origin-pool-apache-85487c5ffb -n user1-test
create_form: null
metadata: null
object: null
replace_form: null
resource_version: ""
spec: null
status:
- conditions:
  - hostname: ""
    last_update_time: "2021-02-24T07:27:55.604060076Z"
    reason: ""
    service_name: ""
    status: Success
    type: Validation
  - hostname: ""
    last_update_time: "2021-02-24T07:27:55.604060076Z"
    reason: ""
    service_name: ""
    status: Installed
    type: Operational
  metadata:
    creation_timestamp: null
    creator_class: pa4-par-dev
    creator_id: ver
    publish: STATUS_PUBLISH
    status_id: ba087263-fd1a-4096-953a-b56e2a064c87_VerCfgMgr
    uid: c808930f-0c4b-471d-b31f-a3670e269804
  object_refs:
  - kind: ves.io.vega.cfg.adc.endpoint.Object
    name: ""
    namespace: ""
    tenant: ""
    uid: 7103712d-0f46-451a-ba54-a07746240a71
  ver_status:
  - allocated_ip:
      addr: ff::19:5200:bc15
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 95.216.26.30
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: pa4-par-dev
  - allocated_ip:
      addr: ff::19:5200:c4a2
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 207.244.88.140
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: pa4-par-dev
- conditions:
  - hostname: ""
    last_update_time: "2021-02-24T07:30:03.858953038Z"
    reason: ""
    service_name: ""
    status: Success
    type: Validation
  - hostname: ""
    last_update_time: "2021-02-24T07:30:03.858953038Z"
    reason: ""
    service_name: ""
    status: Installed
    type: Operational
  metadata:
    creation_timestamp: null
    creator_class: pa4-par-dev
    creator_id: ver
    publish: STATUS_PUBLISH
    status_id: b279f777-7aaf-4c3a-b01a-25186d6b1400_VerCfgMgr
    uid: 1cab3ee8-eab5-4c72-9212-3099f116f7f0
  object_refs:
  - kind: ves.io.vega.cfg.adc.endpoint.Object
    name: ""
    namespace: ""
    tenant: ""
    uid: 7103712d-0f46-451a-ba54-a07746240a71
  ver_status:
  - allocated_ip:
      addr: ff::19:5200:bc15
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 95.216.26.30
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: pa4-par-dev
  - allocated_ip:
      addr: ff::19:5200:c4a2
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 207.244.88.140
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: pa4-par-dev
- conditions:
  - hostname: ""
    last_update_time: "2021-02-24T07:35:01.554259904Z"
    reason: ""
    service_name: ""
    status: Success
    type: Validation
  - hostname: ""
    last_update_time: "2021-02-24T07:35:01.554259904Z"
    reason: ""
    service_name: ""
    status: Installed
    type: Operational
  metadata:
    creation_timestamp: null
    creator_class: pa4-par-dev
    creator_id: ver
    publish: STATUS_PUBLISH
    status_id: b7b6fee5-4fa2-4f9e-904e-c84695cebf19_VerCfgMgr
    uid: 53163f26-13f6-49df-8723-177227fdc9b2
  object_refs:
  - kind: ves.io.vega.cfg.adc.endpoint.Object
    name: ""
    namespace: ""
    tenant: ""
    uid: 7103712d-0f46-451a-ba54-a07746240a71
  ver_status:
  - allocated_ip:
      addr: ff::19:5200:bc15
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 95.216.26.30
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: pa4-par-dev
  - allocated_ip:
      addr: ff::19:5200:c4a2
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 207.244.88.140
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: pa4-par-dev
- conditions:
  - hostname: ""
    last_update_time: "2021-02-23T18:26:21.944978794Z"
    reason: ""
    service_name: ""
    status: Success
    type: Validation
  - hostname: ""
    last_update_time: "2021-02-23T18:26:21.944978794Z"
    reason: ""
    service_name: ""
    status: Installed
    type: Operational
  metadata:
    creation_timestamp: null
    creator_class: re01
    creator_id: ver
    publish: STATUS_PUBLISH
    status_id: f20f8b64-0bd1-4cea-900c-26421c1527d9_VerCfgMgr
    uid: 74f9ea20-453a-4fd8-9137-c9c35baeebab
  object_refs:
  - kind: ves.io.vega.cfg.adc.endpoint.Object
    name: ""
    namespace: ""
    tenant: ""
    uid: 7103712d-0f46-451a-ba54-a07746240a71
  ver_status:
  - allocated_ip:
      addr: ff::18:2800:10
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 207.244.88.140
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: re01
  - allocated_ip:
      addr: ff::18:2800:11
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 95.216.26.30
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: re01
- conditions:
  - hostname: ""
    last_update_time: "2021-02-24T07:29:44.078915641Z"
    reason: ""
    service_name: ""
    status: Success
    type: Validation
  - hostname: ""
    last_update_time: "2021-02-24T07:29:44.078915641Z"
    reason: ""
    service_name: ""
    status: Installed
    type: Operational
  metadata:
    creation_timestamp: null
    creator_class: re02
    creator_id: ver
    publish: STATUS_PUBLISH
    status_id: 5ba5078a-e8dc-4b61-bbf5-97986f6eadcf_VerCfgMgr
    uid: a0a1cda2-6750-4a7b-bb67-157cafeb0f5f
  object_refs:
  - kind: ves.io.vega.cfg.adc.endpoint.Object
    name: ""
    namespace: ""
    tenant: ""
    uid: 7103712d-0f46-451a-ba54-a07746240a71
  ver_status:
  - allocated_ip:
      addr: ff::1e00:31cf
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 95.216.26.30
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: re02
  - allocated_ip:
      addr: ff::1e00:6875
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 207.244.88.140
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: re02
- conditions:
  - hostname: ""
    last_update_time: "2021-02-24T07:30:03.861350810Z"
    reason: ""
    service_name: ""
    status: Success
    type: Validation
  - hostname: ""
    last_update_time: "2021-02-24T07:30:03.861350810Z"
    reason: ""
    service_name: ""
    status: Installed
    type: Operational
  metadata:
    creation_timestamp: null
    creator_class: pa4-par-dev
    creator_id: ver
    publish: STATUS_PUBLISH
    status_id: 628fd05f-64b7-4d92-a1b0-fd3fa2ebcb0f_VerCfgMgr
    uid: b0b6af1b-a41b-419d-bdd8-3726a74256b5
  object_refs:
  - kind: ves.io.vega.cfg.adc.endpoint.Object
    name: ""
    namespace: ""
    tenant: ""
    uid: 7103712d-0f46-451a-ba54-a07746240a71
  ver_status:
  - allocated_ip:
      addr: ff::19:5200:bc15
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 95.216.26.30
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: pa4-par-dev
  - allocated_ip:
      addr: ff::19:5200:c4a2
    discovered_info: null
    discovered_ip:
      ipv4:
        addr: 207.244.88.140
    discovered_port: 80
    health_status: {}
    service_name: ""
    site: pa4-par-dev
system_metadata: null

# vesctl --config ~/.vesconfig.demo1.testcorp configuration status virtual_host ves-io-http-loadbalancer-apache -n user1-test --at-site user1-igw-demo1
conditions:
- hostname: master-0
  last_update_time: "2021-02-24T18:37:57.225890447Z"
  reason: ""
  service_name: ver
  status: Success
  type: Validation
- hostname: master-0
  last_update_time: "2021-02-24T18:37:57.225890447Z"
  reason: ""
  service_name: ver
  status: Installed
  type: Operational
error:
  code: EOK
  error_obj: null
  message: ""


```

## `request` Command

```
$ vesctl request --help
Execute Commands

Usage:
  vesctl request [command]

Examples:
vesctl request secrets encrypt --policy-doc temp_policy --public-key pub_key secret

Available Commands:
  rpc         RPC Invocation
  secrets     Execute commands for secret_management

Flags:
  -h, --help   help for request

Global Flags:
  -a, --cacert string         Server CA cert file path
  -c, --cert string           Client cert file path
      --config string         A configuration file to use for API gateway URL and credentials (default "/Users/harshadnakil/.vesconfig")
  -k, --key string            Client key file path
      --outfmt string         Output format for command
  -o, --output string         Output dir for command (default "./")
      --p12-bundle string     Client P12 bundle (key+cert) file path. Any password for this file should be in environment variable VES_P12_PASSWORD
  -u, --server-urls strings   API endpoint URL (default [http://localhost:8001])
      --show-curl             Emit requests from program in CURL format
      --timeout int           Timeout (in seconds) for command to finish (default 5)

Use "vesctl request [command] --help" for more information about a command.
```

Request command is mechanism to provide custom commands and custom API(s), that do not fit within the structure of configuration objects.

* `rpc` command provides all the custom API(s) provided by Volterra
* `secrets` command provides various operation to upload blindfold secrets to be used by Volterra services and tenant services

### `request rpc` Command

Generic syntax for rpc command is

```
vesctl request rpc rpc-name --uri path --http-method method -i input yaml-file
```

* `rpc-name` is a name of the custom API service e.g `known_label.CustomAPI.Get`
* `path` for custom API e.g /public/namespaces/shared/known_labels
* `method` is HTTP method like `GET` or `POST`
* `yaml-file` will have either query parameters or post body

```
$ vesctl request rpc known_label.CustomAPI.Get --uri /public/namespaces/shared/known_labels --http-method GET -i kl.yaml 
Response:
label:
- description: 'Fleet label for fleet acmecorp superstore fleet'
  key: ves.io/fleet
  value: superstore
- key: app
  value: productpage
- key: ves.io/region
  value: ves-io-london
- key: ves.io/region
  value: ves-io-frankfurt
- key: ves.io/region
  value: ves-io-osaka
- key: ves.io/country
  value: ves-io-uk
- key: ves.io/region
  value: ves-io-sanjose
- key: ves.io/region
  value: ves-io-toronto
- key: ves.io/country
  value: ves-io-sgp
- key: ves.io/region
  value: ves-io-seattle
- key: ves.io/country
  value: ves-io-usa
- key: ves.io/country
  value: ves-io-jpn
- key: ves.io/country
  value: ves-io-fra
- key: ves.io/country
  value: ves-io-ukr
- key: ves.io/region
  value: ves-io-amsterdam
- key: ves.io/region
  value: ves-io-singapore
- key: ves.io/region
  value: ves-io-ashburn
- key: ves.io/region
  value: ves-io-newyork
- key: ves.io/country
  value: ves-io-cze
- key: ves.io/region
  value: ves-io-ohio
- key: ves.io/country
  value: ves-io-isr
- key: ves.io/country
  value: ves-io-can
- key: ves.io/region
  value: ves-io-tokyo
- key: ves.io/country
  value: ves-io-ind
- key: ves.io/region
  value: ves-io-paris
- key: ves.io/country
  value: ves-io-ger
```



### `request secrets` Command

Secrets description is coming soon.
